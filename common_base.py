import __init__
import numpy as np
import time

def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    return init_np.astype(float)

class AugmentReward(object):
    def __init__(self, rew_scale):
        self.func = None
        self.rew_scale = rew_scale

    def get_reward(self, **kwargs):
        return self.func(**kwargs)


class ResultSummaryBase(object):
    def __init__(self):
        self.hist = {}
        self.reset_hist()

    def cal_result(self):
        raise NotImplementedError("cal_result is not implemented")

    def reset_hist(self):
        raise NotImplementedError("reset_hist is not implemented")

    def set_summary(self, env, done, rew):
        raise NotImplementedError("get_summary is not implemented")

    def get_hist(self):
        return self.hist       

class RollOutBase(object):
    def __init__(self, maxstep, clear, render, sleep, aug_rew, agent, env):
        self.maxstep = maxstep
        self.summary = None
        self._render = render
        self._clear = clear
        self._sleep = sleep 
        self.aug_rew = aug_rew
        self.env = env
        self.agent = agent

    def reset_env(self):
        self.env.reset() 
    def sleep(self, sleep):
        self._sleep = sleep

    def clear(self, clear):
        self._clear = clear
 
    def render(self, render):
        self._render = render

    def summary_reset(self):
        self.summary.reset_hist()      

    def get_result(self):
        return self.summary.cal_result()

    def go(self, env, agent, need_hist):
        raise NotImplementedError("go is not implemented")

    def sample(self, env, agent, need_hist):
        raise NotImplementedError("go is not implemented")

class RollOutBaseOld(object):
    def __init__(self, maxstep, clear, render, sleep):
        self.maxstep = maxstep
        self.summary = None
        self._render = render
        self._clear = clear
        self._sleep = sleep 

    def reset_env(self):
        self.env.reset() 
    def sleep(self, sleep):
        self._sleep = sleep

    def clear(self, clear):
        self._clear = clear
 
    def render(self, render):
        self._render = render


    def summary_reset(self):
        self.summary.reset_hist()      

    def get_result(self):
        return self.summary.cal_result()

    def go(self, env, agent=None, need_hist=False):
        raise NotImplementedError("go is not implemented")


