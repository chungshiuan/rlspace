class A(object):
    def __init__(self, i):
        self.i = i
    def geti(self):
        return self.i

class B(object):
    def __init__(self):
        self.i = None
    def addi(self):
        self.i+=1

class C(A,B):
    def __init__(self,i):
        B.__init__(self)
        A.__init__(self, i)
    
