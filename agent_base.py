import __init__
import os, time
import tensorflow as tf
import numpy as np
from net_base import Summary
from tensorflow.python import debug as tf_debug
from net_base import PG, VNet, QNet, A2CNet, A2CNetCEL


INITIAL_EPSILON = 0.5 # starting value of epsilon
FINAL_EPSILON = 0.01 # final value of epsilon


class AgentBase(object):
    def __init__(self, env, model_path, logs_path, learnrate, debug, namespace, det_action, gd, activation, need_greedy):
        self.env = env
        self.saver = None
        self.model_path = model_path
        self.debug = debug
        self.namespace = namespace
        self.sm = Summary(logs_path)
        self.net = None
        self.gd = gd
        self.loop = 0
        self.det_action = det_action
        self.ep = 1
        self.need_greedy = need_greedy
        self.sess = tf.Session()

    def init_session(self):
        self.net.init()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)
        if self.debug:
            self.sess = tf_debug.TensorBoardDebugWrapperSession(self.sess, "localhost:8887")
        self.init_summary()

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.saver.restore(self.sess, self.model_path)
        self.net.load()
        self.init_summary()

    def init_summary(self):
        self.sm.init_writer()
        self.sm_pg = self.sm.get_summary_merge(self.namespace)

    def close_session(self):
        if self.sess is not None:
            self.sess.close()

    def set_need_greedy(self, need_greedy):
        self.need_greedy = need_greedy

    def get_acts(self, obses):
        if self.need_greedy:
            return self.get_greedy_action(obses)
        else:
            return self.get_action(obses)

    def get_greedy_action(self, obses):
        if np.random.uniform(0,1) <= self.ep:
            action = self.env.action_space.sample()
        else:
            action = self.get_action(obses)
        self.ep -= (INITIAL_EPSILON - FINAL_EPSILON)/self.gd 
        return action

    def get_action(self, obses):
        raise NotImplementedError("get_action is not implemented")

    def train(self, obses, acts, rews):
        raise NotImplementedError("train is not implemented")

    def plus_loop(self):
        self.loop += 1

    def save(self):
        self.saver = tf.train.Saver()
        self.saver.save(self.sess, self.model_path)   



class PGAgent(AgentBase):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, optimizer='ada'):
        super(PGAgent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy)

        self.net = PG(env=env, namespace=namespace, learnrate=learnrate, activation=activation, optimizer=optimizer)


class QAgent(AgentBase):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma, optimizer='ada'):
        super(QAgent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy)
        self.gamma = gamma
        self.net = QNet(env=env, namespace=namespace, learnrate=learnrate, activation=activation, optimizer=optimizer)

class A2CAgent(AgentBase):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma, optimizer='ada'):
        super(A2CAgent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy)
        self.gamma = gamma
        self.critic_ns = '{}_critic'.format(namespace)
        self.actor_ns = '{}_actor'.format(namespace)
        self.critic = VNet(env=env, 
                           namespace=self.critic_ns, 
                           learnrate=learnrate, 
                           activation=activation,
                           gamma=gamma,
                           optimizer=optimizer)
        self.actor = PG(env=env, 
                        namespace=self.actor_ns, 
                        learnrate=learnrate, 
                        activation=activation,
                        optimizer=optimizer)

    def init_summary(self):
        self.sm.init_writer()
        self.sm_actor = self.sm.get_summary_merge(self.actor_ns)
        self.sm_critic = self.sm.get_summary_merge(self.critic_ns)
        
    def init_session(self):
        self.actor.init()
        self.critic.init()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)
        if self.debug:
            self.sess = tf_debug.TensorBoardDebugWrapperSession(self.sess, "localhost:8887")
        self.init_summary()

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.actor.load()
        self.critic.load()

    def get_critic_target(self, obses, rews, dones):
        target = []
        v = self.get_critic_v(obses)
        for i, d in enumerate(dones):
#            if d:
#                target.append(rews[i])           
#            else:
                target.append(rews[i]+self.gamma*v[i,0])
        return target

class ShareA2CAgent(AgentBase):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma, optimizer='ada'):
        super(ShareA2CAgent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy)
        self.gamma = gamma
        self.critic_ns = '{}_critic'.format(namespace)
        self.actor_ns = '{}_actor'.format(namespace)
        self.net = A2CNet(env=env, 
                          namespace=namespace, 
                          learnrate=learnrate, 
                          activation=activation, 
                          critic_ns = self.critic_ns, 
                          actor_ns = self.actor_ns,
                          optimizer=optimizer)

    def init_summary(self):
        self.sm.init_writer()
        self.sm_actor = self.sm.get_summary_merge(self.actor_ns)
        self.sm_critic = self.sm.get_summary_merge(self.critic_ns)
        print('+++++++++++++++++++++++++++++++')
        print(self.sm_actor) 
        print('+++++++++++++++++++++++++++++++------------------') 
        print(self.sm_critic) 
        print('+++++++++++++++++++++++++++++++') 

    def init_session(self):
        self.net.init()
        self.sess = tf.Session()
        init = tf.group( tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init)
        if self.debug:
            self.sess = tf_debug.TensorBoardDebugWrapperSession(self.sess, "localhost:8887")
        self.init_summary()

    def load(self):
        self.saver = tf.train.import_meta_graph(self.model_path+'.meta')
        self.sess = tf.Session()
        self.saver.restore(self.sess, self.model_path)
        self.net.load()

    def get_critic_target(self, obses, rews, dones):
        target = []
        v = self.get_critic_v(obses)
        for i, d in enumerate(dones):
            if d:
                target.append(rews[i])           
            else:
                target.append(rews[i]+self.gamma*v[i,0])
        return target

class ShareA2CAgentCEL(ShareA2CAgent):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma, optimizer='ada'):
        super(ShareA2CAgentCEL, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy, 
                                    gamma=gamma)
        self.net = A2CNetCEL(env=env, 
                          namespace=namespace, 
                          learnrate=learnrate, 
                          activation=activation, 
                          critic_ns = self.critic_ns, 
                          actor_ns = self.actor_ns,
                          optimizer=optimizer)



