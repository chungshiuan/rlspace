class PlayerBase(object):
    def __init__(self,env, agent, gamma, render, rollout):
        self.env = env
        self.agent = agent
        self.gamma = gamma
        self.render = render
        self.rollout = rollout

    def play(self, need_load, round):
        if need_load:
            self.agent.load()
        self.rollout.sleep(0)
        self.rollout.clear(False)
        self.rollout.render(self.render)
        print('start to play')
        self.rollout.summary_reset() 
        for i in range(round):
            self.rollout.go(need_hist=True)
        self.env.close()

    def get_discount_sum_of_rewards(self, rewards):
        raise NotImplementedError("get_discount_sum_of_rewards is not implemented")

    def train(self, epoch, need_greedy, reward_type):
        raise NotImplementedError("train is not implemented")

class QPlayerBase(PlayerBase):
    def __init__(self,env, agent, gamma, render, rollout, data_len, batch_size):
        super(QPlayerBase, self).__init__(env, agent, gamma, render, rollout)
        self.dataset = []
        self.data_len = data_len
        self.batch_size = batch_size

    def renew_dataset(self, dt_tmp):
        self.dataset.extend(dt_tmp)
        if len(self.dataset) > self.data_len:
            self.dataset = self.dataset[len(self.dataset)-self.data_len:]
#        print('dataset length: {}'.format(len(self.dataset)))

