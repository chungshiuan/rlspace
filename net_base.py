import tensorflow as tf



Optimizer = {
    'ada': tf.train.AdamOptimizer,
    'rms': tf.train.RMSPropOptimizer
}

class NetBase(object):
    def __init__(self, env, namespace, learnrate, activation, optimizer):
        self.env = env
        self.namespace = namespace 
        self.q = None
        self.loss = None
        self.optimize = None
        self.loop=0
        self.learnrate = learnrate
        self.activation = activation
        self.optimizer = Optimizer.get(optimizer, tf.train.AdamOptimizer)

    def init(self):
        self.set_placeholder()
        self.net()
        self.set_optimize()

    def load(self):
        print('all trainable:', tf.trainable_variables())
        print('all dense:', [ t.name for t in  tf.trainable_variables() if 'dense' in t.name])
        print('all dense:', tf.get_default_graph().get_collection(name='dens2'))
        print('all loss:', [ t.name for t in  tf.get_default_graph().get_operations() if 'loss' in t.name])
        print('all optimize:', [ t.name for t in  tf.get_default_graph().get_operations() if 'optimize' in t.name])
        print('all diffq:', [ t.name for t in  tf.get_default_graph().get_operations() if 'diff' in t.name])
        print('all targ:', [ t.name for t in  tf.get_default_graph().get_operations() if 'targ' in t.name])
        print('all out_prob:', [ t.name for t in  tf.get_default_graph().get_operations() if 'out_prob' in t.name])
      
    def set_placeholder(self):
        raise NotImplementedError("setPlaceHolder is not implemented")

    def net(self):
        with tf.variable_scope(self.namespace):
            self.n_out = tf.layers.dense(self.obses, 
                                units=36, 
#                                activation=tf.nn.relu,
                                activation=getattr(tf.nn, self.activation, None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense1')     
            ''' 
            self.n_out = tf.layers.dense(self.obses, 
                                units=36, 
                                activation=getattr(tf.nn, self.activation, None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense3')       
            '''
            self.n_out = tf.layers.dense(self.n_out, 
                                activation=getattr(tf.nn, self.activation, None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                units=self.env.action_space.n, 
                                name='dense2')
        with tf.variable_scope('{}/'.format(self.namespace)):
            self.n_out = tf.identity(self.n_out, name='n_out')
            tf.summary.histogram('n_out', self.n_out)
            self.out_prob = tf.nn.softmax(self.n_out, name='out_prob')
            tf.summary.histogram('out_prob', self.out_prob)
        
    def set_optimize(self):
        raise NotImplementedError("optimize is not implemented")

    def plus_loop(self):
        self.loop+=1


class Summary(object):
    def __init__(self, logs_path):
        self.path =logs_path
        self.writer = None

    def init_writer(self):
        self.writer = tf.summary.FileWriter(self.path, graph=tf.get_default_graph())
       
    def add_summary(self, *args):
        self.writer.add_summary(*args)

    def get_summary_merge(self, key):
        print('******************', key)
        return tf.summary.merge_all(key='summaries', scope=key)

class PG(NetBase):
    def __init__(self, env, namespace, learnrate, activation, optimizer):
        super(PG, self).__init__(env, namespace, learnrate, activation, optimizer=optimizer)
 
    def load(self):
        super().load()
        self.obses = tf.get_default_graph().get_tensor_by_name('{}/obses:0'.format(self.namespace))
        self.acts = tf.get_default_graph().get_tensor_by_name('{}/acts:0'.format(self.namespace))
        self.rews = tf.get_default_graph().get_tensor_by_name('{}/rews:0'.format(self.namespace))
        self.n_out = tf.get_default_graph().get_tensor_by_name('{}/n_out:0'.format(self.namespace))
        self.out_prob = tf.get_default_graph().get_tensor_by_name('{}/out_prob:0'.format(self.namespace))
        self.loss = tf.get_default_graph().get_tensor_by_name('{}/loss:0'.format(self.namespace))
        self.optimize = tf.get_default_graph().get_operation_by_name('{}/optimize'.format(self.namespace))
 
    def set_placeholder(self):
        with tf.variable_scope(self.namespace): 
            self.obses = tf.placeholder(tf.float32, shape=[None, self.env.observation_space.n], name='obses')
            self.acts = tf.placeholder(tf.int32, name='acts')
            self.rews = tf.placeholder(tf.float32, name='rews')
       
    def set_optimize(self):
        self.optimize_section_CEL()

    def optimize_section(self):
        with tf.variable_scope('{}/'.format(self.namespace)):
            self.out = - tf.reduce_sum(tf.log(self.out_prob)*tf.one_hot(self.acts, self.env.action_space.n), axis=1)
            tf.summary.scalar('rews', self.rews)
            self.loss = tf.reduce_mean(self.out * self.rews, name='loss')
            tf.summary.scalar('loss', self.loss)
            optimizer = self.optimizer(self.learnrate)
            self.optimize = optimizer.minimize(self.loss, name='optimize')
    
    def optimize_section_CEL(self):
        with tf.variable_scope('{}/'.format(self.namespace)):
            # to maximize total reward (log_p * R) is to minimize -(log_p * R), and the tf only have minimize(loss)
            print(tf.cast(tf.one_hot(self.acts, self.env.action_space.n),tf.int64))
            self.out = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.n_out, 
                                                                  labels=self.acts)   # this is negative log of chosen action
            # or in this way:
            # neg_log_prob = tf.reduce_sum(-tf.log(self.all_act_prob)*tf.one_hot(self.tf_acts, self.n_actions), axis=1)
            self.loss = tf.reduce_mean(self.out * self.rews, name='loss')  
            tf.summary.scalar('loss', self.loss)
            optimizer = self.optimizer(self.learnrate)
            self.optimize = optimizer.minimize(self.loss, name='optimize')

class QNet(NetBase):
    def __init__(self, env, namespace, learnrate, activation, optimizer):
        super(QNet, self).__init__(env, namespace, learnrate, activation, optimizer=optimizer)

    def load(self):
        super().load()
        self.obses = tf.get_default_graph().get_tensor_by_name('{}/obses:0'.format(self.namespace))
        self.acts = tf.get_default_graph().get_tensor_by_name('{}/acts:0'.format(self.namespace))
        self.rews = tf.get_default_graph().get_tensor_by_name('{}/rews:0'.format(self.namespace))
        self.n_out = tf.get_default_graph().get_tensor_by_name('{}/n_out:0'.format(self.namespace))
        self.out_prob = tf.get_default_graph().get_tensor_by_name('{}/out_prob:0'.format(self.namespace))
        self.loss = tf.get_default_graph().get_tensor_by_name('{}/loss:0'.format(self.namespace))
        self.optimize = tf.get_default_graph().get_operation_by_name('{}/optimize'.format(self.namespace))
        self.target = tf.get_default_graph().get_tensor_by_name('{}/targ:0'.format(self.namespace))
        self.diffQ = tf.get_default_graph().get_tensor_by_name('{}/diffq:0'.format(self.namespace))

    def set_placeholder(self):
        with tf.variable_scope(self.namespace):
            self.obses = tf.placeholder(tf.float32, shape=[None, self.env.observation_space.n], name='obses')
            self.acts = tf.placeholder(tf.int32, shape=[None, ], name='acts')
            self.rews = tf.placeholder(tf.float32, shape=[None, ], name='rews')
            self.target = tf.placeholder(tf.float32, shape=[None,], name='targ')
            self.success = tf.placeholder(tf.float32, name='success')

    def set_optimize(self):
        with tf.variable_scope('{}/'.format(self.namespace)):
            Q_action = tf.reduce_sum(tf.multiply(self.n_out, tf.one_hot(self.acts, depth=self.env.action_space.n, dtype=tf.float32)), reduction_indices = 1)
            self.diffQ = tf.identity(self.target - Q_action, name='diffq')
            self.loss = tf.reduce_mean(tf.square(self.diffQ), name='loss')
            self.optimize = self.optimizer(self.learnrate).minimize(self.loss, name='optimize')
            tf.summary.scalar("loss", self.loss)

class VNet(QNet):
    def __init__(self, env, namespace, learnrate, activation, gamma, optimizer):
        super(QNet, self).__init__(env, namespace, learnrate, activation, optimizer=optimizer)

    def load(self):
        super().load()
        self.obses = tf.get_default_graph().get_tensor_by_name('{}/obses:0'.format(self.namespace))
        self.acts = tf.get_default_graph().get_tensor_by_name('{}/acts:0'.format(self.namespace))
        self.rews = tf.get_default_graph().get_tensor_by_name('{}/rews:0'.format(self.namespace))
        self.loss = tf.get_default_graph().get_tensor_by_name('{}/loss:0'.format(self.namespace))
        self.optimize = tf.get_default_graph().get_operation_by_name('{}/optimize'.format(self.namespace))
        self.target = tf.get_default_graph().get_tensor_by_name('{}/targ:0'.format(self.namespace))
        self.diffQ = tf.get_default_graph().get_tensor_by_name('{}/diffq:0'.format(self.namespace))

    def net(self):
        with tf.variable_scope(self.namespace):
            self.n_out = tf.layers.dense(self.obses, 
                                units=36, 
                                activation=getattr(tf.nn, self.activation, None),
#                                activation=tf.nn.relu,
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense1')     
            self.n_out = tf.layers.dense(self.n_out, 
                                activation=getattr(tf.nn, self.activation, None),
#                                activation=None,
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                units=1, 
                                name='dense2')
        with tf.variable_scope('{}/'.format(self.namespace)):
            self.n_out = tf.identity(self.n_out, name='n_out')
            tf.summary.histogram('n_out', self.n_out)
            tf.summary.histogram('obs', tf.argmax(self.obses))

    def set_optimize(self):
        with tf.variable_scope('{}/'.format(self.namespace)):
            self.diffQ = tf.identity(self.target - self.n_out, name='diffq')

            self.loss = tf.reduce_mean(tf.square(self.diffQ), name='loss')
            self.optimize = self.optimizer(self.learnrate).minimize(self.loss, name='optimize')
            tf.summary.scalar("loss", self.loss)
class Obj:
    def __init__(self):
        pass

class A2CNet(QNet):
    def __init__(self, env, namespace, learnrate, activation, critic_ns, actor_ns, optimizer):
        super(A2CNet, self).__init__(env, namespace, learnrate, activation, optimizer=optimizer)
        self.critic_ns = critic_ns
        self.actor_ns = actor_ns
        self.critic = Obj()
        self.actor = Obj()
        self.weights = None
        self.bias = None

    def load(self):
        super().load()
        self.obses = tf.get_default_graph().get_tensor_by_name('{}/obses:0'.format(self.namespace))
        self.acts = tf.get_default_graph().get_tensor_by_name('{}/acts:0'.format(self.namespace))
        self.rews = tf.get_default_graph().get_tensor_by_name('{}/rews:0'.format(self.namespace))
        self.target = tf.get_default_graph().get_tensor_by_name('{}/targ:0'.format(self.namespace))
        self.diffQ = tf.get_default_graph().get_tensor_by_name('{}/diffq:0'.format(self.actor_ns))
        self.critic.n_out = tf.get_default_graph().get_tensor_by_name('{}/critic_n_out:0'.format(self.critic_ns))
        self.critic.loss = tf.get_default_graph().get_tensor_by_name('{}/critic_loss:0'.format(self.critic_ns))
        self.critic.optimize = tf.get_default_graph().get_operation_by_name('{}/critic_optimize'.format(self.critic_ns))
        self.actor.n_out = tf.get_default_graph().get_tensor_by_name('{}/actor_n_out:0'.format(self.actor_ns))
        self.actor.loss = tf.get_default_graph().get_tensor_by_name('{}/actor_loss:0'.format(self.actor_ns))
        self.actor.optimize = tf.get_default_graph().get_operation_by_name('{}/actor_optimize'.format(self.actor_ns))

    def net(self):
        with tf.variable_scope(self.namespace):
            self.n_out = tf.layers.dense(self.obses, 
                                units=32, 
#                                activation=tf.nn.relu,
                                activation=getattr(tf.nn, self.activation, None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense1')   
#            self.weight1 = tf.get_default_graph().get_tensor_by_name('{}/dense1/kernel:0'.format(self.namespace)) 
            self.n_out = tf.layers.dense(self.n_out, 
                                units=64, 
#                                activation=tf.nn.relu,
                                activation=getattr(tf.nn, self.activation, None),
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                name='dense4')   
#            self.weight1 = tf.get_default_graph().get_tensor_by_name('{}/dense1/kernel:0'.format(self.namespace)) 

            self.n_out = tf.layers.dense(self.n_out, 
                                activation=None,
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
                                bias_initializer=tf.constant_initializer(0.1), 
                                units=self.env.action_space.n,
                                name='dense3')
#            self.weight2 = tf.get_default_graph().get_tensor_by_name('{}/dense3/kernel:0'.format(self.namespace))

            self.n_out = tf.identity(self.n_out, name='n_out')
            self.critic.n_out = tf.layers.dense(self.n_out, 
                                activation=tf.nn.tanh,
                                kernel_initializer=tf.random_normal_initializer(mean=0, stddev=2),
                                bias_initializer=tf.constant_initializer(0.1), 
                                units=1,
                                name='dense2')
#            self.weight3 = tf.get_default_graph().get_tensor_by_name('{}/dense2/kernel:0'.format(self.namespace))

            self.actor.n_out = tf.nn.softmax(tf.nn.tanh(self.n_out))

        with tf.variable_scope('{}/'.format(self.critic_ns)):
            self.critic.n_out = tf.identity(self.critic.n_out, name='critic_n_out')
            tf.summary.histogram('critic_n_out', self.critic.n_out)

        with tf.variable_scope('{}/'.format(self.actor_ns)):
            self.actor.n_out = tf.identity(self.actor.n_out, name='actor_n_out')
            tf.summary.histogram('actor_n_out', self.actor.n_out)

    def set_optimize(self):
        optimizer = self.optimizer(self.learnrate)
        with tf.variable_scope('{}/'.format(self.actor_ns)):
            self.actor.act_one_hot = tf.one_hot(self.acts, self.env.action_space.n)
            self.actor.log_out = tf.log(self.actor.n_out + 1e-10) 
            self.actor.out = (-1) * tf.reduce_sum(self.actor.log_out*self.actor.act_one_hot, axis=1)
            self.actor.outrew = self.actor.out*self.rews
            self.actor.loss = tf.reduce_mean(self.actor.outrew, name='actor_loss')
#            self.grad1 = tf.gradients(self.actor.loss, self.weight1)
#            print('?????????????????/',self.grad1)
#            self.grad2 = tf.gradients(self.actor.loss, self.weight2)
#            self.grad3 = tf.gradients(self.actor.loss, self.weight3)
            self.actor.optimize = optimizer.minimize(self.actor.loss, name='actor_optimize')
            tf.summary.scalar('actor_loss', self.actor.loss)

        with tf.variable_scope('{}/'.format(self.critic_ns)):
            self.diffQ = tf.identity(self.target - tf.squeeze(self.critic.n_out,axis=1), name='diffq')
            self.critic.loss = tf.reduce_mean(tf.square(self.diffQ), name='critic_loss')
            self.critic.optimize = optimizer.minimize(self.critic.loss, name='critic_optimize')
            tf.summary.scalar("critic_loss", self.critic.loss)

class A2CNetCEL(A2CNet):
    def __init__(self, env, namespace, learnrate, activation, critic_ns, actor_ns, optimizer):
        super(A2CNetCEL, self).__init__(env, namespace, learnrate, activation, critic_ns, actor_ns, optimizer=optimizer)

    def set_optimize(self):
        optimizer = self.optimizer(self.learnrate)
        with tf.variable_scope('{}/'.format(self.actor_ns)):
            self.actor.out = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=self.n_out, 
                                                                  labels=self.acts)
            self.actor.loss = tf.reduce_mean(self.actor.out * self.rews, name='actor_loss')  
            self.actor.optimize = optimizer.minimize(self.actor.loss, name='actor_optimize')
            tf.summary.scalar('actor_loss', self.actor.loss)

        with tf.variable_scope('{}/'.format(self.critic_ns)):
            self.diffQ = tf.identity(self.target - tf.squeeze(self.critic.n_out,axis=1), name='diffq')
            self.critic.loss = tf.reduce_mean(tf.square(self.diffQ), name='critic_loss')
            self.critic.optimize = optimizer.minimize(self.critic.loss, name='critic_optimize')
            tf.summary.scalar("critic_loss", self.critic.loss)


class A2CSplitNet(A2CNet):
    def __init__(self, env, namespace, learnrate, activation, critic_ns, actor_ns, optimizer):
        super(A2CSplitNet, self).__init__(env, namespace, learnrate, activation, critic_ns, actor_ns, optimizer=optimizer)
        self.critic = VNet(env=env, 
                           namespace=self.critic_ns, 
                           learnrate=learnrate, 
                           activation=activation,
                           gamma=gamma,
                           optimizer=optimizer)
        self.actor = PG(env=env, 
                        namespace=self.actor_ns, 
                        learnrate=learnrate, 
                        activation=activation,
                        optimizer=optimizer)
    def load(self):
        self.critic.load()
        self.actor.load()

    def set_optimize(self):
        self.critic.set_optimize()
        self.actor.set_optimize()

def get_entropy(logits):
    a_0 = logits - tf.reduce_max(logits, axis=-1, keepdims=True)
    exp_a_0 = tf.exp(a_0)
    z_0 = tf.reduce_sum(exp_a_0, axis=-1, keepdims=True)
    p_0 = exp_a_0 / z_0
    return tf.reduce_sum(p_0 * (tf.log(z_0) - a_0), axis=-1)    
         
class A2CNetStableBaseLine(A2CNet):
    def __init__(self, env, namespace, learnrate, activation, critic_ns, actor_ns, optimizer, ent_coef, v_coef):
        super(A2CNetStableBaseLine, self).__init__(env, namespace, learnrate, activation, critic_ns, actor_ns, optimizer)
        self.ent_coef = ent_coef
        self.v_coef = v_coef

    def load(self):
        super().load()
        self.obses = tf.get_default_graph().get_tensor_by_name('{}/obses:0'.format(self.namespace))
        self.acts = tf.get_default_graph().get_tensor_by_name('{}/acts:0'.format(self.namespace))
        self.rews = tf.get_default_graph().get_tensor_by_name('{}/rews:0'.format(self.namespace))
        self.target = tf.get_default_graph().get_tensor_by_name('{}/targ:0'.format(self.namespace))
        self.diffQ = tf.get_default_graph().get_tensor_by_name('{}/diffq:0'.format(self.actor_ns))
        self.critic.n_out = tf.get_default_graph().get_tensor_by_name('{}/critic_n_out:0'.format(self.critic_ns))
        self.critic.loss = tf.get_default_graph().get_tensor_by_name('{}/critic_loss:0'.format(self.critic_ns))
        self.actor.n_out = tf.get_default_graph().get_tensor_by_name('{}/actor_n_out:0'.format(self.actor_ns))
        self.actor.loss = tf.get_default_graph().get_tensor_by_name('{}/actor_loss:0'.format(self.actor_ns))
        self.optimize = tf.get_default_graph().get_operation_by_name('{}/optimize'.format(self.namespace))

    def set_optimize(self):
        optimizer = self.optimizer(self.learnrate)
        with tf.variable_scope('{}/'.format(self.actor_ns)):
            self.actor.act_one_hot = tf.one_hot(self.acts, self.env.action_space.n)
            self.actor.log_out = tf.log(self.actor.n_out + 1e-10) 
            self.actor.out = (-1) * tf.reduce_sum(self.actor.log_out*self.actor.act_one_hot, axis=1)
            self.actor.outrew = self.actor.out*self.rews
            self.actor.loss = tf.reduce_mean(self.actor.outrew, name='actor_loss')
#            self.grad1 = tf.gradients(self.actor.loss, self.weight1)
#            print('?????????????????/',self.grad1)
#            self.grad2 = tf.gradients(self.actor.loss, self.weight2)
#            self.grad3 = tf.gradients(self.actor.loss, self.weight3)
            tf.summary.scalar('actor_loss', self.actor.loss)
            tf.summary.histogram('self.actor.oo', self.n_out)

        with tf.variable_scope('{}/'.format(self.critic_ns)):
            self.diffQ = tf.identity(self.target - tf.squeeze(self.critic.n_out,axis=1), name='diffq')
            self.diffQ = tf.Print(self.diffQ, ['dddddddddifffffQQQQQQQQQQQQQQ', self.diffQ], summarize=100)
            self.critic.loss = tf.reduce_mean(tf.square(self.diffQ), name='critic_loss')
            tf.summary.scalar("critic_loss", self.critic.loss)

        with tf.variable_scope('{}/'.format(self.namespace)):
            self.entropy = tf.reduce_mean(get_entropy(self.n_out))
            print('>>>>>>>>>>>>.entrioy',self.entropy)
            #loss = self.pg_loss - self.entropy * self.ent_coef + self.vf_loss * self.vf_coef
            self.total_loss = self.actor.loss - self.entropy*self.ent_coef + self.critic.loss*self.v_coef
            tf.summary.scalar("actor_loss", self.actor.loss)
            tf.summary.scalar("entropy", self.entropy)
            tf.summary.scalar("critic_loss", self.critic.loss)
            tf.summary.scalar("total_loss", self.total_loss)
            self.optimize = optimizer.minimize(self.total_loss, name='optimize')

class A2CNetStableBaseLineSplit(A2CNetStableBaseLine):
    def __init__(self, env, namespace, learnrate, activation, critic_ns, actor_ns, optimizer, ent_coef, v_coef):
        super(A2CNetStableBaseLineSplit, self).__init__(env, namespace, learnrate, activation, critic_ns, actor_ns, optimizer, ent_coef, v_coef)


    def load(self):
        super().load()
        self.critic.n_out = tf.get_default_graph().get_tensor_by_name('{}/critic_n_out:0'.format(self.critic_ns))
        self.critic.loss = tf.get_default_graph().get_tensor_by_name('{}/critic_loss:0'.format(self.critic_ns))
        self.actor.n_out = tf.get_default_graph().get_tensor_by_name('{}/actor_n_out:0'.format(self.actor_ns))
        self.actor.loss = tf.get_default_graph().get_tensor_by_name('{}/actor_loss:0'.format(self.actor_ns))

    def net(self):
        with tf.variable_scope(self.actor_ns):
            self.actor.n_out = tf.layers.dense(self.obses, 
                                             units=64, 
                                             activation=getattr(tf.nn, self.activation, None),
                                             kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
#                                             bias_initializer=tf.constant_initializer(0.1), 
                                             bias_initializer=tf.zeros_initializer(), 
                                             name='dense1')  
            self.actor.n_out = tf.layers.dense(self.actor.n_out, 
                                             units=64, 
                                             activation=getattr(tf.nn, self.activation, None),
                                             kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
#                                             bias_initializer=tf.constant_initializer(0.1),
                                             bias_initializer=tf.zeros_initializer(), 
                                             name='dense2')  
            self.actor.n_out = tf.layers.dense(self.actor.n_out, 
                                             units=self.env.action_space.n,
                                             activation=None,
                                             kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
#                                             bias_initializer=tf.constant_initializer(0.1),
                                             bias_initializer=tf.zeros_initializer(), 
                                             name='dense3')  
            self.actor.no_act_out = tf.identity(self.actor.n_out, name='actor_no_act_out')
            self.actor.n_out = tf.nn.softmax(self.actor.n_out)
            self.actor.n_out = tf.identity(self.actor.n_out, name='actor_n_out')
            tf.summary.histogram('actor_n_out', self.actor.n_out)


        with tf.variable_scope(self.critic_ns):
            self.critic.n_out = tf.layers.dense(self.obses, 
                                             units=64, 
                                             activation=getattr(tf.nn, self.activation, None),
                                             kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
#                                             bias_initializer=tf.constant_initializer(0.1),
                                             bias_initializer=tf.zeros_initializer(),
                                             name='dense1')   
            self.critic.n_out = tf.layers.dense(self.critic.n_out, 
                                             units=64, 
                                             activation=getattr(tf.nn, self.activation, None),
                                             kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
#                                             bias_initializer=tf.constant_initializer(0.1),
                                             bias_initializer=tf.zeros_initializer(),
                                             name='dense2')   
            self.critic.n_out = tf.layers.dense(self.critic.n_out, 
                                             units=1,
                                             activation=None,
                                             kernel_initializer=tf.random_normal_initializer(mean=0, stddev=0.3),
#                                             bias_initializer=tf.constant_initializer(0.1),
                                             bias_initializer=tf.zeros_initializer(),
                                             name='dense3')   
            self.critic.n_out = tf.identity(self.critic.n_out, name='critic_n_out')
            tf.summary.histogram('critic_n_out', self.critic.n_out)


    def set_optimize(self):
        optimizer = self.optimizer(self.learnrate)
        with tf.variable_scope('{}/'.format(self.actor_ns)):
            self.actor.act_one_hot = tf.one_hot(self.acts, self.env.action_space.n)
#            self.actor.n_out = tf.Print(self.actor.n_out, ['nnnnnnnnnnnnnnnnnnnnnout', self.actor.n_out], summarize=100)

#            self.actor.log_out = tf.log(self.actor.n_out + 1e-10)

#            self.actor.log_out = tf.Print(self.actor.log_out, ['dddddddddifffffQQQQQQQQQQQQQQ', self.actor.log_out], summarize=100)

#            self.actor.out = (-1) * tf.reduce_sum(self.actor.log_out*self.actor.act_one_hot, axis=1)
            self.actor.out = tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.actor.no_act_out, labels=tf.stop_gradient(self.actor.act_one_hot))
            self.actor.outrew = self.actor.out*self.rews
            tf.summary.histogram('self.actor.out', self.actor.out)
            tf.summary.histogram('self.rews', self.rews)
            mean,variance = tf.nn.moments(x=self.actor.out, axes=[0])
            tf.summary.scalar('mean', mean)
            tf.summary.scalar('variance', variance)
          
            self.actor.loss = tf.reduce_mean(self.actor.outrew, name='actor_loss')
            self.actor.loss = self.actor.loss #+ tf.random_normal(shape=[], mean=0, stddev=10)
            tf.summary.scalar('actor_loss', self.actor.loss)
            tf.summary.histogram('self.actor.no_act_out', self.actor.no_act_out)

        with tf.variable_scope('{}/'.format(self.critic_ns)):
            self.diffQ = tf.identity(self.target - tf.squeeze(self.critic.n_out,axis=1), name='diffq')
            self.critic.loss = tf.reduce_mean(tf.square(self.diffQ), name='critic_loss')
            tf.summary.scalar("critic_loss", self.critic.loss)

        with tf.variable_scope('{}/'.format(self.namespace)):
            self.entropy = tf.reduce_mean(get_entropy(self.actor.no_act_out))
            print('>>>>>>>>>>>>.entrioy',self.entropy)
            #loss = self.pg_loss - self.entropy * self.ent_coef + self.vf_loss * self.vf_coef
            self.total_loss = self.actor.loss - self.entropy*self.ent_coef + self.critic.loss*self.v_coef
            tf.summary.scalar("actor_loss", self.actor.loss)
            tf.summary.scalar("entropy", self.entropy)
            tf.summary.scalar("critic_loss", self.critic.loss)
            tf.summary.scalar("total_loss", self.total_loss)
#            self.optimize = optimizer.minimize(self.total_loss, name='optimize')
            grads_and_vars = optimizer.compute_gradients(self.total_loss)
            grads = [g for g, v in grads_and_vars]
            vs = [v for g, v in grads_and_vars]
            grads, _ = tf.clip_by_global_norm(grads, 0.5)
            grads_and_vars = list(zip(grads, vs))
            print('gradsgradsgrads:', grads_and_vars)
            [tf.summary.histogram("%s-grad" % g[1].name, g[0]) for g in grads_and_vars]
            self.optimize = optimizer.apply_gradients(grads_and_vars)
        self.set_success()

    def set_success(self):
        with tf.variable_scope('success'.format(self.namespace)):
             tf.summary.scalar("success", self.success) 
        
