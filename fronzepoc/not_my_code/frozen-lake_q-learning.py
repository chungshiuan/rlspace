import gym
import __init__

import numpy as np
import time, pickle, os
from utils import save, load, evaluate_policy, check_path

env = gym.make('FrozenLake-v0')

epsilon = 0.3

total_episodes = 10000
count = 0
max_steps = 100

lr_rate = 0.81
gamma = 0.96

Q = np.zeros((env.observation_space.n, env.action_space.n))

def extract_policy(q, gamma = 1.0):
    """ Extract the policy given a value-function """
    policy = np.zeros(16)
    for s in range(16):
        policy[s] = np.argmax(q[s])
    return policy
   
def choose_action(state):
    action=0
    print(count)
    if np.random.uniform(0, 1) < epsilon and count < 4000:

        action = env.action_space.sample()
    else:
        action = np.argmax(Q[state, :])
    return action

def learn(state, state2, reward, action):
    predict = Q[state, action]
    target = reward + gamma * np.max(Q[state2, :])
    Q[state, action] = Q[state, action] + lr_rate * (target - predict)

# Start
for episode in range(total_episodes):
    state = env.reset()
    count += 1
    t = 0
    while t < max_steps:
        action = choose_action(state)  

        state2, reward, done, info = env.step(action)  

        learn(state, state2, reward, action)

        state = state2

        t += 1
#        env.render()
        print('------------------',episode)    
        print(Q)

#        time.sleep(0.1)

#        os.system('clear')
     
        if done:
            break


print(Q)
policy = extract_policy(Q)

policy_score = evaluate_policy(env, policy, gamma, n=1000)

with open("frozenLake_qTable.pkl", 'wb') as f:
    pickle.dump(Q, f)
