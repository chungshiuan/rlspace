import __init__
import numpy as np
import gym, time, os, argparse
from gym import wrappers
from gym import register
import random as pr
from utils import save, load, evaluate_policy, check_path
from fl88env import FLEnv
 

def rargmax(vector):
    """ Argmax that chooses randomly among eligible maximum indices. """
    m = np.amax(vector)
    indices = np.nonzero(vector == m)[0]
    return pr.choice(indices)

def extract_policy(v, gamma = 1.0):
    """ Extract the policy given a value-function """
    policy = np.zeros(env.nS)
    for s in range(env.nS):
        q_sa = np.zeros(env.action_space.n)
        for a in range(env.action_space.n):
            for next_sr in env.P[s][a]:
                # next_sr is a tuple of (probability, next state, reward, done)
                p, s_, r, _ = next_sr
                q_sa[a] += (p * (r + gamma * v[s_]))
        policy[s] = rargmax(q_sa)
    return policy


def value_iteration(env, gamma = 1.0):
    """ Value-iteration algorithm """
    v = np.zeros(env.nS)  
    max_iterations = 200000
    eps = 1e-50
    for i in range(max_iterations):
        prev_v = np.copy(v)
        for s in range(env.nS):
            #q_sa = [sum([p*(r + prev_v[s_]) for p, s_, r, _ in env.P[s][a]]) for a in range(env.nA)]
            q_sa = []
            for a in range(env.nA):
                q_sum = 0
                for p, s_, r, _ in env.P[s][a]:
                    q_sum = q_sum + p*(r+prev_v[s_])
                q_sa.append(q_sum)      
            v[s] = max(q_sa)
        if (np.sum(np.fabs(prev_v - v)) <= eps):
            break
    return v


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t') 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    PATH = check_path( os.path.join('__d_frozenlake',__file__.split('.')[0]))
    PATH = os.path.join(PATH, 'qtable')
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
    #env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
#    env_name = 'FrozenLakeNotSlippery-v0'
    gamma = 1
    env = gym.make(env_name)
    env = env.unwrapped
#    fe = FLEnv()
#    env = fe.get_env()
    if args.td == 'train': 
        optimal_v = value_iteration(env, gamma)
        policy = extract_policy(optimal_v, gamma)
        save(policy, PATH)
    policy = load(PATH)
    policy_score = evaluate_policy(env, policy, gamma, n=1000, render=True)
    print('Policy average score = ', policy_score)
