import __init__
import numpy as np
import time
from common_base import ResultSummaryBase, RollOutBaseOld
import gym
from gym import wrappers
from gym import register
from fl88env import FLEnv

MAXSTEP=200

def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    init_np[np.arange(len(data)), data] = 1
    return init_np.astype(float)

class ResultSummary(ResultSummaryBase):
    def __init__(self):
        super(ResultSummary, self).__init__()

    def cal_result(self):
        return float(self.hist['G'] / (self.hist['G'] + self.hist['H']))

    def reset_hist(self):
        self.hist = {'G':0, 'H':0}
 
    def set_summary(self, env, done, rew):
        if done:
            if rew <= 0:
                self.hist['H'] += 1
                print('T____________T')
            else:
                self.hist['G'] += 1
                print('>____________^')

class RollOut(RollOutBaseOld):
    def __init__(self, maxstep=500, clear=False, render=False, sleep =0):
        super(RollOut, self).__init__(maxstep, clear, render, sleep)
        self.summary = ResultSummary()

    def go(self, env, agent=None, need_hist=False):
        obs, rew, done = env.reset(), 0, False
        obses, actses, rewses = [], [], []
        step=0
        while not done:
            act = None
            if agent is None:
                act = env.action_space.sample()
            else:
                act = agent.get_acts([obs])
            obs_, rew, done, info = env.step(act)
            if self._render:
                env.render()
            if self._clear:
                os.system('clear')
            time.sleep(self._sleep)
            actses.append(act)
            obses.append(obs)
            rewses.append(rew)
            obs = obs_
            if done:
               self.summary.set_summary(env, done, rew)
            if done and need_hist:
               print(self.summary.get_hist())
            step+=1
            if step > self.maxstep:
               print('max step:{} is reached'.format(MAXSTEP))
               break
        return obses, actses, rewses

def rollout(env, agent=None, clear=False, render=False, MAXSTEP=500, need_history=False, sleep=0):
    obs, rew, done = env.reset(), 0, False
    obses, actses, rewses = [], [], []
    step=0
    rs = ResultSummary(render=render, clear=clear, sleep=sleep)
    while not done:
        if agent is None:
            obs_, rew, done, info = env.step(env.action_space.sample())
        else:
            t1 = time.time() 
            prob = agent.get_acts([obs])
            t2 = time.time()
            obs_, rew, done, info = env.step(prob)
            t3=time.time()
#            print(t2-t1)
#            print(t3-t2)
#            actses.append(act)
        obses.append(obs)
#        actses.append(act)
        rewses.append(rew)
        obs = obs_
        if done and need_history:
            print(rs.get_summary(env, done, rew, render, clear ))
        else:
            rs.get_summary(env, done, rew, render, clear )
        step+=1
        if step > MAXSTEP:
           print('max step:{} is reached'.format(MAXSTEP))
           break

    print('------------------- len rews: {}'.format(sum(rewses)))

    return obses, actses, rewses

def get_action(qtable,s, act_type=1, rm_th=0.3):
    if act_type == 0:
        return np.random.choice([0,1,2,3])
    else:
        if np.random.uniform(0,1) <= rm_th:
            return np.random.choice([0,1,2,3])
        return np.argmax(qtable[s])

class Env(object):
    def __init__(self, slip, one_det):
        register(id='FrozenLakeNotSlippery-v0',
                 entry_point='gym.envs.toy_text:FrozenLakeEnv',
                 kwargs={'map_name' : '4x4', 'is_slippery': False},)
        self.env = None
        if slip:
            self.env_name  = 'FrozenLake-v0'
            self.env = gym.make(self.env_name) 
            print('>>>>>>>>>> {} <<<<<<<<<<<<<'.format('frozenlake'))
            if one_det:
                fe = FLEnv()
                env = fe.get_env()
                print('!!!!!!!!!!!! {} !!!!!!!!!!!!'.format('frozenlake, deterministic'))

        else:
            self.env_name = 'FrozenLakeNotSlippery-v0'
            self.env = gym.make(self.env_name) 
            print('============= {} ============='.format('frozenlake, no splip'))

    def get_env(self):
        return self.env







