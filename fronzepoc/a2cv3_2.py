import __init__
import numpy as np
import os, time, argparse, random
from utils import check_path
from rollout import A2CRollOut, AugmentReward
from cp_common import np_one_hot, Env
from agent_base import ShareA2CAgentCEL
from a2cv2_2 import Player as PlayerParent

logs_path = os.path.join('__d_logs',__file__.split('.')[0], str(int(time.time())))
PATH = check_path( os.path.join('__d_model',__file__.split('.')[0]))
PATH = os.path.join( PATH, 'model')

class Agent(ShareA2CAgentCEL):
    def __init__(self, env, model_path, logs_path, debug, namespace, gd, learnrate, det_action, activation, need_greedy, gamma, optimizer):
        super(Agent, self).__init__(env=env, 
                                    model_path=model_path, 
                                    logs_path=logs_path, 
                                    debug=debug, 
                                    namespace=namespace, 
                                    gd=gd, 
                                    learnrate=learnrate, 
                                    det_action=det_action, 
                                    activation=activation,
                                    need_greedy=need_greedy,
                                    gamma=gamma,
                                    optimizer=optimizer)

    def get_action(self, obses):
        if self.net.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.net.obses: np_one_hot(obses, self.env.observation_space.n)}
        prob = self.sess.run([self.net.actor.n_out], feed_dict = feed_dict)
        if self.det_action:
            act = np.argmax(prob[0].ravel())
        else:
            act = np.random.choice(self.env.action_space.n, p=np.squeeze(prob, axis=0))
        return act

    def train_actor(self, obses, acts, rews ):
        feed_dict = {
            self.net.obses : np_one_hot(obses, self.env.observation_space.n),
            self.net.acts: np.array(acts),
            self.net.rews: np.array(rews)
        }
        opt, loss, no, sm_actor, ou, re = self.sess.run([self.net.actor.optimize, 
                                                                      self.net.actor.loss, 
                                                                      self.net.n_out, 
                                                                      self.sm_actor, 
                                                                      self.net.actor.out, 
                                                                      self.net.rews], 
                                                                      feed_dict=feed_dict)
        print('-----------------')
        print('actor n_out:{}'.format(no))
        print('actor out:{}'.format(ou))
        print('actor rew:{}'.format(re))
        print('actor loss:{}'.format(loss))
        print('-----------------')
        self.sm.add_summary(sm_actor, self.loop)       

    def train_critic(self, obses, acts, obses_, rewards, dones):
        feed_dict = {
            self.net.obses : np_one_hot(obses, self.env.observation_space.n),
            self.net.acts: np.array(acts),
            self.net.target: np.array(self.get_critic_target(obses_, rewards, dones)),
            self.net.rews: np.array(rewards),
        }
        opt, loss, out = self.sess.run([self.net.critic.optimize, self.net.critic.loss, self.net.critic.n_out], feed_dict=feed_dict)
        diffq, sm_critic = self.sess.run([self.net.diffQ, self.sm_critic], feed_dict=feed_dict)
        self.sm.add_summary(sm_critic, self.loop)     
        return diffq 

    def get_critic_v(self, obses):
        if self.net.obses is None or self.sess is None:
            print('obses or sess is None')
            return None 
        feed_dict = { self.net.obses: np_one_hot(obses, self.env.observation_space.n)
                    }
        v = self.sess.run(self.net.critic.n_out, feed_dict = feed_dict)
        return v

class Player(PlayerParent):
    def __init__(self,env, agent, gamma, render, rollout, data_len, batch_size):
        super(Player, self).__init__(env=env,
                                            agent=agent,
                                            gamma=gamma,
                                            render=render,
                                            rollout=rollout,
                                            data_len=data_len,
                                            batch_size=batch_size)

    def train(self, epoch=2000):
        print('start to train')
        for i in range(epoch):
            obses, acts, rews, dones, obses_ = self.rollout.sample()
            print('============================================ {} ===== {} =======> {}'.format(i, rews[-1], self.agent.ep, rews[-1]))
            print(obses)
            diffQ = self.agent.train_critic(obses=obses, 
                                            acts=acts, 
                                            obses_=obses_,
                                            rewards=rews, 
                                            dones=dones)
            self.agent.train_actor(obses=obses, acts=acts, rews=diffQ)
            self.agent.plus_loop()
            if i > 1000 and  i % 500 == 0:
                 self.play(need_load=False, round=100)
#                if self.rollout.get_result() >= 0.0:
                 self.agent.save()
        self.agent.close_session()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--type",type=str, help="train or play", default='play')
    parser.add_argument("-c", "--cont", type=str, help="continue exist table", default='t')
    parser.add_argument("-e", "--greedy_denom", type=int, help="greedy_denom", default=10000)
    parser.add_argument("-l", "--learn_rate", type=float, help="learning rate", default=0.0001)
    parser.add_argument("-g", "--need_greedy", type=bool, help="need greedy", default=False)
    parser.add_argument("-i", "--play_load", type=bool, help="player need load", default=True)
    parser.add_argument("--play_render", type=bool, help="player rebder", default=False)
    parser.add_argument("--play_round", type=int, help="player round", default=1000)
    parser.add_argument("--play_clear", type=bool, help="player clear", default=False)
    parser.add_argument("--play_sleep", type=int, help="player sleep", default=0)
    parser.add_argument("--slip", type=bool, help="slipping", default=True)
    parser.add_argument("--one_det", type=bool, help="one deterministic", default=False)
    parser.add_argument("--gamma", type=float, help="gamma", default=0.99)
    parser.add_argument("--train_epoch", type=int, help="train epoch", default=10000)
    parser.add_argument("--reward_type", type=int, help="reward type", default=0)
    parser.add_argument("--det_action", type=bool, help="deterministic action", default=True)
    parser.add_argument("--activation", type=str, help="activation function", default='relu')
    parser.add_argument("--rew_scale", type=int, help="rew_scale", default=1)
    parser.add_argument("--data_len", type=int, help="data length", default=1000)
    parser.add_argument("--batch_size", type=int, help="batch size", default=50)

    args = parser.parse_args()
    print(args)
    isContinue = (args.cont in ['t','true','True', '1'])
    print('=================================================')
    print(args)
    print('=================================================')
    print('isContinue',isContinue)
    env = Env(slip=args.slip, one_det=args.one_det).get_env() 
    agent = Agent(env=env, 
                  model_path=PATH, 
                  logs_path=logs_path, 
                  debug=False, 
                  namespace='qnn', 
                  gd=args.greedy_denom, 
                  learnrate=args.learn_rate,
                  det_action=args.det_action,
                  activation=args.activation,
                  need_greedy=args.need_greedy,
                  gamma=args.gamma)
    if not isContinue:
        agent.init_session()
    else:
        agent.load()
    aug_rew = AugmentReward(selector=args.reward_type, rew_scale=args.rew_scale)
    rollout = A2CRollOut(maxstep=500, 
                        clear=args.play_clear,
                        render=args.play_render,
                        sleep=args.play_sleep,
                        aug_rew=aug_rew,
                        agent=agent,
                        env=env)
    player = Player(env=env, 
                    agent=agent,
                    gamma=args.gamma,
                    render=args.play_render,
                    rollout=rollout,
                    data_len=args.data_len, 
                    batch_size=args.batch_size)
    if args.type == 'train':
        player.train(epoch=args.train_epoch)
    elif args.type == 'play':
        player.agent.set_need_greedy(need_greedy=False)
        player.play(need_load=args.play_load, 
                    round=args.play_round)
    else:
        print('No options')
    env.close()        

