import __init__
from argment import ArgmentBase


class ArgmentA2C(ArgmentBase):
    def __init__(self):
        super(ArgmentA2C, self).__init__()

    def add_specific(self): 
        self.parser.add_argument("--slip", type=bool, help="slipping", default=True)
        self.parser.add_argument("--one_det", type=bool, help="one deterministic", default=False)

