import __init__
import numpy as np
import gym, time, os, argparse
from gym import wrappers
from gym import register
from utils import save, load, evaluate_policy, check_path
from fl88env import FLEnv

hist = {'G':0, 'H':0}

def rargmax(vector):
    """ Argmax that chooses randomly among eligible maximum indices. """
    m = np.amax(vector)
    indices = np.nonzero(vector == m)[0]
    return pr.choice(indices)


def extract_policy(v, gamma = 1.0):
    """ Extract the policy given a value-function """
    policy = np.zeros(env.nS)
    for s in range(env.nS):
        q_sa = np.zeros(env.nA)
        for a in range(env.nA):
            for p, s_, r, _ in env.P[s][a]:
               q_sa[a] = q_sa[a] + (p * (r + gamma * v[s_]))
        policy[s] = np.argmax(q_sa)
    return policy

def compute_policy_v(env, policy, gamma=1.0):
    """ Iteratively evaluate the value-function under policy.
    Alternatively, we could formulate a set of linear equations in iterms of v[s] 
    and solve them to find the value function.
    """
    v = np.zeros(env.nS)
    eps = 1e-8
    while True:
        delta = 0
        prev_v = np.copy(v)
        for s in range(env.nS):
            policy_a = policy[s]
            su = []
            for p, s_, r, _ in env.P[s][policy_a]:
               su.append(p * (r + gamma * prev_v[s_]))
            v[s] = sum(su)
            delta = max(delta,np.abs(v[s]-prev_v[s]))
        if delta <= eps:
            break
    return v

def policy_iteration(env, gamma = 1.0):
    """ Policy-Iteration algorithm """
    policy = np.random.choice(env.nA, size=(env.nS))  # initialize a random policy
    max_iterations = 200000
    gamma = 1.0
    for i in range(max_iterations):
        old_policy_v = compute_policy_v(env, policy, gamma)
        new_policy = extract_policy(old_policy_v, gamma)
        if (np.all(policy == new_policy)):
            print ('Policy-Iteration converged at step %d.' %(i+1))
            break
        policy = new_policy
    return policy


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--td", type=str, help="train or play", default='play')
    parser.add_argument("-c", "--co", type=str, help="continue exist table", default='t') 
    args = parser.parse_args()
    isContinue = (args.co in ['t','true','True', '1'])
    PATH = check_path( os.path.join('__d_frozenlake',__file__.split('.')[0]))
    PATH = os.path.join(PATH, 'qtable')
    register(
            id='FrozenLakeNotSlippery-v0',
            entry_point='gym.envs.toy_text:FrozenLakeEnv',
            kwargs={'map_name' : '4x4', 'is_slippery': False},
            )
    #env_name  = 'FrozenLake8x8-v0'
    env_name  = 'FrozenLake-v0'
    #env_name = 'FrozenLakeNotSlippery-v0'
    env = gym.make(env_name)
    env = env.unwrapped
#    fe = FLEnv()
#    env = fe.get_env()
    if args.td == 'train': 
        optimal_policy = policy_iteration(env, gamma = 1.0)
        save(optimal_policy, PATH)
    optimal_policy = load(PATH)
    scores = evaluate_policy(env, optimal_policy, gamma = 1.0, n=1000)
    print('Average scores = ', np.mean(scores))
