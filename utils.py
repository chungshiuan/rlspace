import __init__
import numpy as np
import time, os, pickle
import random as pr
from fnmatch import fnmatch
from tensorflow.python.client import device_lib
import numpy as np


hist = {'G':0, 'H':0}

def np_one_hot(data, type_num):
    init_np = np.zeros((len(data), type_num))
    print('>>>>>>>>>>>>>>>',data)
    init_np[np.arange(len(data)), data] = 1

    return init_np

def get_pattern_files(path, pattern='training'):
    ret = []
    for item in os.listdir(path):
        if fnmatch(item, '*'+pattern+'*'):
            ret.append(os.path.join(path, item))
    return ret

def check_path(path):
    path=os.path.abspath(path)
    try:
        if not os.path.exists(path):
            os.makedirs(path)    
        return path
    except Exception as e:
        print('check {} fail: {}'.format(path, e))
        return None


def get_available_gpus(d_type='GPU'):
    local_device_protos = device_lib.list_local_devices()
    return [x.name for x in local_device_protos if x.device_type == d_type]

def pixel_range(img):
    vmin, vmax = np.min(img), np.max(img)
    if vmin * vmax >= 0:
        return (vmin, vmax)
    else:
        if -vmin > vmax:
            vmax = -vmin
        else:
            vmin = -vmax
    return (vmin, vmax)

def rargmax(vector):
    """ Argmax that chooses randomly among eligible maximum indices. """
    m = np.amax(vector)
    indices = np.nonzero(vector == m)[0]
    return pr.choice(indices)

def run_episode(env, policy, gamma = 1.0, render = False, run_slower=False):
    """ Runs an episode and return the total reward """
    obs = env.reset()
    total_reward = 0
    step_idx = 0
    while True:
        if render:
            env.render()
        obs, reward, done , _ = env.step(int(policy[obs]))
        total_reward += (gamma ** step_idx * reward)
        step_idx += 1
        if done:
            env.render()
            if reward <= 0:
               hist['H'] += 1
               print('T_______T')
            else:
               hist['G'] += 1
               print('^_______<')
            break
        if run_slower:
            time.sleep(1)
    print('success:{} , fail: {}'.format(hist['G'], hist['H']))
#        os.system('clear')
    return total_reward

def get_summary(env, done, rew, render=True, clear=True, sleep=0):
    if render:
        env.render()
    time.sleep(sleep)
    if clear:
        os.system('clear')
    if done:
        if rew <= 0:
            hist['H'] += 1
            print('T____________T')
        else:
            hist['G'] += 1
            print('>____________^')
    return hist       
 
def evaluate_policy(env, policy, gamma = 1.0,  n = 100, render= True):
    """ Evaluates a policy by running it n times.
    returns:
    average total reward
    """
    scores = [
            run_episode(env, policy, gamma = gamma, render = render)
            for _ in range(n)]
    return np.mean(scores)


def save(Q, path):
    folder = os.path.join(*path.split('/')[0: -1])
#    check_path(folder)
    with open(path, 'wb') as f:
        pickle.dump(Q, f)
 
def load(path):
    with open(path, 'rb') as f:
        return pickle.load(f)

if __name__ == '__main__':
   print(get_available_gpus())
   print(get_available_gpus('CPU'))
